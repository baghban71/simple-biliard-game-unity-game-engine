﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootAI : MonoBehaviour {

    public GameObject mainBall;
    public GameObject ball_1;
    GameObject sp1,sp2;

    Rigidbody rb;
	// Use this for initialization
	void Start () {
        sp1 = GameObject.Find("ball");
        sp2 = GameObject.Find("ball_01");

       // var angle = 45*Mathf.Deg2Rad ;
        var power = 4000;
        float angle = getAngle(sp1, sp2);
        print(angle * Mathf.Rad2Deg);
        Vector2 translatePos = new Vector2();
        translatePos.x = -Mathf.Sin(angle) * power;
        translatePos.y = Mathf.Cos(angle) * power;

        rb = mainBall.GetComponent<Rigidbody>();
        rb.AddForce(translatePos.x, 0, translatePos.y);
	}

    float getAngle(GameObject obj1, GameObject obj2)
    {
        return Mathf.Atan2(obj1.transform.position.z - obj2.transform.position.z, obj1.transform.position.x - obj2.transform.position.x) + (Mathf.PI / 2)+(-3f*Mathf.Deg2Rad);//(Mathf.PI/2) is the telorance, =90 degree
      
    }
	// Update is called once per frame
	void Update () {
		
	}
}
