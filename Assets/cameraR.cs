﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraR : MonoBehaviour {


	void Start ()
	{
		swingOpen ();
	}

	void Update ()
	{
		
	}

	void swingOpen()
	{
		Quaternion newRotation = new Quaternion (transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w);
		newRotation *= Quaternion.Euler (0, 90, 0);
		transform.rotation = Quaternion.Slerp (transform.rotation, newRotation, 20 * Time.deltaTime);
	}
}